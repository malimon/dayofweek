#! /bin/bash 

# usage: ./dayofweek.sh month day weekday

month=$1;
mday=$2;
weekday=$3;
for year in $(seq 2019 -1 1997);do 
  date -j -f '%m-%d-%Y' "$month-$mday-$year" +'%Y %A';
done | egrep "$weekday"
